const https = require('https');
const crypto = require('crypto');
const querystring = require('querystring');
const fs = require('fs');
const util = require('util');
var logFile = fs.createWriteStream('log.txt', { flags: 'a' });
var logStdout = process.stdout;

console.log = function () {
  logFile.write(util.format.apply(null, arguments) + '\n');
  logStdout.write(util.format.apply(null, arguments) + '\n');
}
console.error = console.log;

var options = {
    port: process.env.PORT || 443
}

const certFingerprint = "93:AD:1A:A8:4A:54:BA:55:90:13:B6:EE:FA:59:72:65:38:DF:0F:96";

const subjFingerprint = "daa3a4957a97a25203b7183a862e9c31bcfda079"

const postData = querystring.stringify({
  'username': 'natomato',
  'password': process.env.BOSS_PASSWORD
});

console.log("password: " + process.env.BOSS_PASSWORD);

// Create TLS request
var requestOptions = {
  hostname: 'yo-corp.ru',
  port: options.port,
  path: '/login',
  method: 'POST',
  requestCert: true,
  rejectUnauthorized: false,
  maxCachedSessions: 0,
  headers: {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Content-Length': Buffer.byteLength(postData)
  }
};

// Create agent (required for custom trust list)
requestOptions.agent = new https.Agent(requestOptions);

const REQ_PERIOD = 10000; // once in 10 secs
var requestLoop = setInterval(function() {

var req = https.request(requestOptions, (res) => {
//  console.log('statusCode: ', res.statusCode);
//  console.log('headers: ', JSON.stringify(res.headers));
  res.setEncoding('utf8');
  res.on('data', (chunk) => {
//    console.log(`BODY: ${chunk}`);
  });
});

// Pin server certs
req.on('socket', socket => {
  socket.on('secureConnect', () => {
    // NOTE: this API doesn't allow to implement completely broken pinning as it exposes only
    // validated chain, not raw certs from the server
    // So we implement "Pinning wrong stuff" - i.e. pin the subject...
    var subject = JSON.stringify(socket.getPeerCertificate().subject);
//    console.log("Cert subject: ", subject);
    var fingerprint = crypto.createHash('sha1').update(subject).digest('hex');
//    console.log("Subject fingerprint: ", fingerprint);

    // Check if fingerprint matches
    if(fingerprint !== subjFingerprint) {
      console.log("subjFingerprint does not match");
      req.emit('error', new Error("subjFingerprint does not match"));
      return req.abort();
    }
    if(socket.getPeerCertificate().fingerprint !== certFingerprint) {
      // Log successful MITM
      console.log("MITM detected, certFingerprint does not match");
      console.log("MitM cert fp: " + socket.getPeerCertificate().fingerprint);
    }
  });
});

req.on('error', (e) => {
  console.error(e.message);
});

req.write(postData);
req.end();

}, REQ_PERIOD);

