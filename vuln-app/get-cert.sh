#!/usr/bin/bash
if [ -z "$1" ]; then
    echo "Usage: get-cert.sh hostname"
    exit 1
fi

echo -n | openssl s_client -connect $1:443 | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' > "$1-cert.pem"
echo "Downloaded certificate to $1-cert.pem file"