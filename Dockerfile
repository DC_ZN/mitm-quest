FROM alpine:edge
ADD ./vuln-app /app
WORKDIR /app
RUN apk add --no-cache nodejs nodejs-npm \
  && npm install
CMD ["node", "vuln-app.js"]

